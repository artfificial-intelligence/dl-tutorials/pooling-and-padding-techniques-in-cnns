# CNN에서의 Pooling과 Padding 기법 <sup>[1](#footnote_1)</sup>

> <font size="3">컨벌루션 신경망이 이미지 데이터 처리에 어떻게 사용되는지 알아본다.</font>

## 목차

1. [개요](./pooling-and-padding-techniques.md#intro)
1. [풀링 이란?](./pooling-and-padding-techniques.md#sec_02)
1. [풀링 타입](./pooling-and-padding-techniques.md#sec_03)
1. [패딩 이란?](./pooling-and-padding-techniques.md#sec_04)
1. [패딩 타입](./pooling-and-padding-techniques.md#sec_05)
1. [풀링과 패딩의 장점과 단점](./pooling-and-padding-techniques.md#sec_06)
1. [요약](./pooling-and-padding-techniques.md#summary)

<a name="footnote_1">1</a>: [DL Tutorial 6 — Pooling and Padding Techniques in CNNs](https://medium.com/nerd-for-tech/dl-tutorial-6-pooling-and-padding-techniques-in-cnns-e9c2e11dbebf?sk=e7e3fd8b61916957545d763c79ec64b3)를 편역하였습니다.
